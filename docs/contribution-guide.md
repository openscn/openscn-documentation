# Contributing

## How can I contribute ? 

### Research and documentation

In case you have experience working with IoT at any scale and want to share experience, guides and scripts we would love to learn about it
and host your documentation! please contact as at [hello@openscn.io](mailto:hello@opensscn.io)  

### Code

Although this project is not mature enough to support a typical process, all the issues at our repositories are up for grubs. Our community
is available at our slack channel. Should you have any questions, feel free to contact us directly

### UX / Designs

Non of our team members are expert at UX. We host our graphical work is hosted at the openscn/openscn-management repository. For any comments
on user experience you can open an issue directly at the openscn/openscn-frontend repository 

## Installation

Until we create a solution for easy deployments, I will list the individual components that you will need in order to have a running instance

### WEB-API 

The openscn-web-api is a api that holds all of the project's functionality for now. That of course is supposed to change when the need for
better resources management arrives.

Please follow the instructions at the [openscn/openscn-web-api](https://gitlab.com/openscn/openscn-web-api) to get started

### Web client

The openscn-frontend is the web client for the openscn project

Please follow the instructions at the [openscn/openscn-frontend](https://gitlab.com/openscn/openscn-frontend) to get started

## Development

Please check out the [openscn/openscn-web-api](https://gitlab.com/openscn/openscn-web-api) and [openscn/openscn-frontend](https://gitlab.com/openscn/openscn-frontend) for more information 
