![OpenSCN][image_1.0]

<!-- # OpenSCN -->

Open Smart Campus Network - A self hosted IoT cloud

We are an inclusive community of students with interest in the IoT field and open source! We love to learn and grow together!


If you're interested in the code, visit our repositories at [gitlab](https://gitlab.com/openscn)

## Our goals

This project has two main goals. The immediate target is to provide a learning platform for us and our colleagues who are researching,
practicing or experiment with IoT technologies. From the perspective OpenSCN is a platform that allow student to test and enhance their skills
in coding, but also a tool that help students skip the software part of their IoT project and focus on their main objective.

The long term goal is to provide the open source community with a platform that is able to manage IoT solutions that scale beyond the home
automation scope. Not everyone is able to manage their own IoT infrastructure, but everyone can benefit from having their data close to them
and independent from the major IoT backend providers.

## Road map

1. ✅ Create a first implementation that can serve the needs for individuals who have tech experience
2. ✅ Create an MVP that can support the needs of multiple people at the scale of a building
3. 👉 Transform the OpenSCN to a product that can be easily deployed, extended and managed by technical people
4. ⌛ Reach out to the community, get feedback and improve this project
5. ⌛ Figure out next steps given the progress 😁

## How can you help

### Join the community

If you are interested in this project please join our [slack channel](https://join.slack.com/t/openscn/shared_invite/zt-fulbaioi-nNr3nMvsMpnC4WYQRdeOjg) or reach out to [hello@openscn.io](mailto:hello@openscn.io)

### Mentor us

The most valuable contribution for us right now is to meet people with experience in the open source project and community management,
people with technical skills that can mentor us or people with experience in the IoT business sector. If you are that person, we would love
to have a chat with you


## Meet the community

Right now all of our members are located in Thessaloniki, Greece and are students of the International Hellenic University
We would love to meet you and learn about your experience with IoT!
Feel free to [join the community](https://join.slack.com/t/openscn/shared_invite/zt-fulbaioi-nNr3nMvsMpnC4WYQRdeOjg) and say Hello!

[image_1.0]: img/openscn-logo-with-font.png "OpenSCN"
