# Infrastructure

## High level description <a name="high-level-description"></a>

The Smart Campus infrastructure is a collection of systems. Every of the subsystems can be replaced by other implementation, duplicated or extended without any change at the rest of the components. A brief schematic of the system is shown at the image 2.1.

![A high level overview of OpenSCN][image_2.1]
Image 2.1: IoT devices are connected via the projects API to the event parser component and saved to the database. The user is able to access that data through the web API


## Data model <a name="data-model"></a>

@todo update me

- teams: a team with at least one member having projects.
- users: a physical person, can be part of a team and have own projects.
- projects: a set of devices and group of sensors producing values.
- device: a single device with a uuid having sensors.
- group: a set of sensors producing the correlated values.
- sensors: a single sensor.

## Projects API <a name="projects-api"></a>

The projects API is component that is being targeted by the IoT devices.
Once a request has reached the the API, it is being authenticated and any information about that request is being retrieved from the database.

If everything goes as well, the component will inform the event parser for the incoming request.

As projects API is considered any component of the system that can handle incoming requests, parse them and pass them to the event parser at specific format. Thus that component may have multiple instances and it's implementation may varies to match needs of a variety of technologies such as other protocols.


## Event parser <a name="event-parser"></a>

The event parser component is the component that executes any job that should be executed for a received value (typically store it to a database).

In the future it should accomplish  more difficult tasks such as update subscribed (to a project) users.


## WEB API <a name="web-api"></a>

The Web API is the portal for a user to our system. Through that API a user can see/consume the  data the other projects are producing. Also they can create teams and manage their projects, devices and sensors.


## Front end <a name="front-end"></a>

The front end application does not provide any other functionality to the project. It's use is optional.

[image_2.1]: img/architecture.jpg "A high level overview of OpenSCN"
