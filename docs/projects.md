# Projects

At this list we track the physical projects that are using our infrastructure as their back end. If you are using our infrastructure for
your project, feel free to reach out and send us more details by either opening an issue to [openscn/openscn-documentation](https://gitlab.com/openscn/openscn-documentation) or by email to
[hello@openscn.io](mailto:hello@openscn.io)

## Currently on development

 - Room air quality monitoring


## Deployed

- N/A 
