FROM nginx:alpine as builder

ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 make && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools

RUN mkdir /app/
WORKDIR /app
COPY ./requirements.txt /app
RUN pip3 install -r ./requirements.txt

COPY ./docs /app/docs
COPY ./Makefile /app/Makefile
COPY ./mkdocs.yml /app/mkdocs.yml

RUN make

FROM nginx:alpine as production

COPY --from=builder /app/site /usr/share/nginx/html
